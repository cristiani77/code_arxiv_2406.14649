function res=Gu(uL,uR)

if uL>=uR
  res=max(fluxu(uL),fluxu(uR));
else
  if uR<0
    res=fluxu(uR);
  elseif uL>0
    res=fluxu(uL);
  else
    res=0;
  end
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%
function res=fluxu(u)
res=0.5*u*u;