%TEST 1 in paper ArXiv:2406.14649
%AUTHOR: EMILIANO CRISTIANI, CNR-IAC, June 2024

clear all
clc
close all

global fmax sigma

%grid
Nx=100;                   %number of space steps
Xmin=0;                   %domain, lower bound
Xmax=100;                 %domain, upper bound
Tfin=10*60;               %final time
Dx=(Xmax-Xmin)/Nx;        %space step
Dt=Dx/2;                  %time step
Nt=round(Tfin/Dt);        %number of time steps
X=Xmin+Dx/2:Dx:Xmax-Dx/2; %barycentric grid

%parameters
epsilon=0.0; 
alphap=1;    
alpham=0.1; 
beta=1;
gamma=0.01;  
nu=0.1;      
delta=1;      %vision cone (in cells, not meters!)
umin=-1.5;
umax=1;
taumin=1;
taumax=5.5;
fmax=0.5;
sigma=0.5;

%zerofy
RHO=zeros(Nx,Nt); TAU=RHO; U=RHO; %solutions
OMEGA=RHO; THETA=RHO;             %additional functions
FLUXexpost=RHO;                   %for the computation of fundamental diagram ex post

%initial conditions
RHO(:,1)=0.5*(X<=40);
TAU(:,1)=taumin;
U(:,1)  =0; 

%% CYCLE IN TIME
for n=1:Nt-1
        
   if (n<300) RHO(1,n)=0.5; end %left boundary condition

   %theta (nonlocal term)
   for j=1:Nx-1
      int=0;
      for i=1:delta
         k=j+i; if (k>=Nx) k=Nx; end
         int=int+TAU(k,n);
      end
      int=int/delta;
      THETA(j,n)=-((int-nu)-RHO(j,n));
   end

   %% CYCLE IN SPACE
   for j=2:Nx-1

        %RHO
        RHO(j,n+1) = RHO(j,n) - (Dt/Dx)*(Grho(j,j+1,n,RHO,TAU)-Grho(j-1,j,n,RHO,TAU));
       

        %U
        if THETA(j,n)<0
           OMEGA(j,n) = - epsilon*U(j,n) + alpham*THETA(j,n); 
        else
           dTHETAjn = (THETA(j,n)-THETA(j-1,n))/Dx;
           OMEGA(j,n) = - epsilon*U(j,n) + alphap*max(0,THETA(j,n)-beta*dTHETAjn); 
        end
        U(j,n+1) = U(j,n) - (Dt/Dx)*( Gu(U(j,n),U(j+1,n)) - Gu(U(j-1,n),U(j,n)) ) + Dt*OMEGA(j,n);
        
        %bound on U
        if (U(j,n+1)<umin) U(j,n+1)=umin; end
        if (U(j,n+1)>umax) U(j,n+1)=umax; end


        %TAU
        TAU(j,n+1) = TAU(j,n) + gamma*Dt*U(j,n); 
          
        %bound on tau
        if (TAU(j,n+1)>taumax)      TAU(j,n+1)=taumax;     end
        if (TAU(j,n+1)<taumin)      TAU(j,n+1)=taumin;     end 
        if (TAU(j,n+1)<RHO(j,n+1))  TAU(j,n+1)=RHO(j,n+1); end

        %Fundamental Diagram ex post
        FLUXexpost(j,n)=Grho(j,j+1,n,RHO,TAU);
      
   end %end of cycle in space

    
    % GATE between jgate e jgate+1
    jgate=66;
    if n<800
       RHO(jgate,n+1)=TAU(jgate,n+1);       
       RHO(jgate+1:end,n+1)=0;
       U(jgate+1:end,n+1)=0;
       TAU(jgate+1:end,n+1)=taumin;
    end

    
end %end of cycle in time
    

%% PLOT
figure
for n=1:5:Nt
   plot(X,U(:,n),'gx',X,RHO(:,n),'b.',X,TAU(:,n),'r_',X,zeros(Nx),'k:'); hold on;
   title(['$t$=',num2str((n-1)*Dt),' sec ($n$=',num2str(n),')' ],'Interpreter','Latex')
   xlabel('space $x$ (m)','Interpreter','Latex')
   axis([Xmin Xmax -2 4]);
   plot([X(jgate) X(jgate)],[-10 10],'k:'); 
   legend({'$u$ (ped/m/sec)','$\rho$ (ped/m)','$\tau$ (ped/m)'},'Interpreter','Latex','Position',[.2 .8 .1 .1])
   text(58,3.5,'gate');
   hold off;
   drawnow;
   pause(0.1);
end


%figure;
figure('units','normalized','outerposition',[0 0 1 1])
hold on; set(gcf,'color','w');
axis([0 2.5 0 0.55])
for j=2:Nx-1
    if j~=jgate
       plot(RHO(j,1:2:Nt),FLUXexpost(j,1:2:Nt),'.')  
    end
end