function res=Grho(jleft,jright,n,RHO,TAU)
   res = min( S(jleft,n,RHO,TAU) , R(jright,n,RHO,TAU) );
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%
function res=S(j,n,RHO,TAU)
global fmax sigma
   if RHO(j,n)<sigma
      res=fluxrho(RHO(j,n),TAU(j,n));
   else
      res=fmax;
   end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%
function res=R(j,n,RHO,TAU)
global fmax sigma
   if RHO(j,n)<sigma
      res=fmax;
   else
      res=fluxrho(RHO(j,n),TAU(j,n));
   end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%
function res=fluxrho(rho,tau)
global fmax sigma
   if rho<=sigma 
      res=(fmax/sigma)*rho;
   else 
      res=((rho-tau)/(sigma-tau))*fmax;
   end
end